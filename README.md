# OpenML dataset: Amazon-Stock-Price-1997-to-2020

https://www.openml.org/d/43516

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description
Amazon.com, Inc., is an American multinational technology company based in Seattle that focuses on e-commerce, cloud computing, digital streaming, and artificial intelligence. It is considered one of the Big Four technology companies, along with Google, Apple, and Facebook.
Context
Time series forecasting for the Amazon Stock Prices. Explore the Data.
About the dataset
Date - in format: yy-mm-dd
Open - the price of the stock at market open 
High - Highest price reached in the day
Low - Lowest price reached in the day
Close - The stock closing at the end of the Market hours
Adj Close - Is the closing price after adjustments for all applicable splits and dividend distributions.
Volume - Number of shares traded
Content
What's inside is more than just rows and columns. Make it easy for others to get started by describing how you acquired the data and what time period it represents, too.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43516) of an [OpenML dataset](https://www.openml.org/d/43516). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43516/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43516/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43516/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

